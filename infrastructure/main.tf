variable "workspace_iam_role" {
  type    = string
  default = ""
}

variable "session_name" {
  type    = string
  default = "local"
}

terraform {
  backend "s3" {
    bucket         = "samuel-terraform-state"
    key            = "Rest-API-Playground"
    region         = "us-east-2"
    dynamodb_table = "terraform_state_lock"
    encrypt        = true
    role_arn       = "arn:aws:iam::452625543413:role/Terraform_Backend"
  }
}

provider "aws" {
  region = "us-east-2"
  assume_role {
    role_arn     = var.workspace_iam_role
    session_name = var.session_name
  }
}

# DATA
data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_vpc" "vpc" {
  tags = {
    Terraform = true
    Name      = "Main VPC"
  }
}

data "aws_subnet_ids" "private_subnet_ids" {
  vpc_id = data.aws_vpc.vpc.id

  tags = {
    Private   = true
    Terraform = true
  }
}

data "aws_subnet_ids" "public_subnet_ids" {
  vpc_id = data.aws_vpc.vpc.id

  tags = {
    Public    = true
    Terraform = true
  }
}

data "aws_subnet" "public_subnets" {
  for_each = toset(data.aws_subnet_ids.public_subnet_ids.ids)

  id = each.key
}

data "aws_subnet" "private_subnets" {
  for_each = toset(data.aws_subnet_ids.private_subnet_ids.ids)

  id = each.key
}