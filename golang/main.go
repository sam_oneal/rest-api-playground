package main

import "gitlab.com/sam_oneal/rest-api/cmd"

func main() {
	s, err := cmd.NewServer()

	if err != nil {
		panic(err)
	}

	s.Start()
}
