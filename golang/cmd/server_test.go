package cmd

import (
	"testing"
)

func TestServer(t *testing.T) {
	s, err := NewServer()

	if err != nil {
		t.Errorf("NewServer() == expected no error but got %q", err.Error())
	}

	err = s.Start()

	if err != nil {
		t.Errorf("Start() == expect no error but got %q", err.Error())
	}
}
