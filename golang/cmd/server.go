package cmd

// Server struct
type Server struct{}

// NewServer create a new server struct
func NewServer() (*Server, error) {
	return &Server{}, nil
}

// Start starts the server
func (s *Server) Start() error {
	println("Starting server ...")
	return nil
}
