package com.gitlab.sam_oneal

interface PirateTranslator {
    fun translate(message: String): String
}