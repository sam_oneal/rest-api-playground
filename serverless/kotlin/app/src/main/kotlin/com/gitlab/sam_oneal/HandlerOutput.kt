package com.gitlab.sam_oneal

data class HandlerOutput(val message: String, val pirateMessage: String)